package debugging.assignment;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test Company class
 */
public class CompanyTest
{
    /**
     * Checks for different employees
     */
    @Test
    public void testDifferentEmployees()
    {
        Employee[] employees = new Employee[] { new Employee("Dan", 12345), new Employee("Gabriela", 123), new Employee("Andrew", 12)};

        Company c1 = new Company(employees);

        // change it so that it's a different company

        employees[0].setEmployeeId(12);

        /*used for debugging:
        for(int i=0;i<c1.getEmployees().length;i++){
            System.out.println(c1.getEmployees()[i].getEmployeeId());
        }*/


        Company c2 = new Company(employees);
        assertNotEquals(c1, c2);

    }
}
